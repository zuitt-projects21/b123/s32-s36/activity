const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
//bcrypt has methods which will help us add a layer of security for our users' passwords.

//import the auth module and deconstruct to get createAccessToken method
const auth = require('../auth');
const {createAccessToken} = auth;

module.exports.registerUserController = (req,res) => {

	console.log(req.body);

	if(req.body.password.length < 8) return res.send({message: "Password is too short"});
		//simple validation. but most validation is done in the backend.

	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);

	//hashedPW returns an encrypted password: ex) $2b$10$YI925JTxO7YYejyZd1.oHe72Lmo5oZxHbqTw1p3kP5qdkRAabG2Je
	/*bcrypt adds a layer of security to our passwords, it hashes our password string into a randomized character version of your password.
	  
	  it is able to hide your password within that randomized string.

	  syntax: bcrypt.hashSync(<stringToBeHashed>,<saltrounds>)
	  	**note: it takes a lot of resources to hash things esp if >10 yung salt rounds
	  	**also note: 2 different hashes can match the same string.
	*/

	User.findOne({username: req.body.username})
		.then( result => {
			if(result !== null && result.username === req.body.username){

				return res.send("Duplicate username found!")

			} else{

				let newUser = new User({

					username: req.body.username,
					password: hashedPW,
					email: req.body.email,
					firstName: req.body.firstName,
					lastName: req.body.lastName,
					mobileNo: req.body.mobileNo

				})

				newUser.save()
					.then(result => {
						 res.send(result)
					})
					.catch(err => res.send(err))					
			}	
		})

		.catch(err => res.send(err))
		
}

module.exports.loginUser = (req,res) => {
	/*
		1. find the user by the email
		2. if user is found, we will check his password
		3. if no found user, sends message to client instead
		4. If user found > pw matches, will generate a 'key'.
		   If user found > pw does not match, sends a message back to client	
	*/

	User.findOne({email: req.body.email})
		.then(result => {
			
			if(result === null){
				res.send("User not found");
			}else {
				//result returns an object containing the user document 
				/*console.log(req.body.email);
				console.log(req.body.password);*/

				const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);
					//syntax: bcrypt.compareSync(<String>,<hashedString>)
						//compareSync returns a boolean.
				console.log(isPasswordCorrect);

				if(isPasswordCorrect){
					// console.log("We will generate a key.")
					return res.send({accessToken: createAccessToken(result)});
				}else{
					return res.send({message: "Password incorrect"})
				}

			}
			
		})	

		.catch(err => res.send(err))
}

module.exports.viewUser = (req,res) => {

	//logged in user's details after decoding with auth module's verify()
	console.log(req.user)

	//using req.user.id would mean that you wont need to enter eh database id in the endpoint anymore. will instead show the details of the logged in user with the same endpoint	User.findById(req.user.id)
		.then(result => res.send(result))
		.catch(err => res.send(err))
}

module.exports.updateProfile = (req,res) => {

	//user id will be accessed after verify. no need for req.params.id

	let updates = {

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo

	}

	User.findByIdAndUpdate(req.user.id,updates,{new:true})
		.then(updatedUser => res.send(updatedUser))
		.catch(err => res.send(err))

}
/*
	async and await
		JS doesnt wait for your variable to finish processing before continuing. it will either stop the whole process or just continue forward.

		With the use of async and await we can wait for the process to finish before we go forward.

		We add async keyword to make a function asynchronous.

		NOTE: await can only be used on async functions
	
*/
		
module.exports.enroll = async (req,res) => {
	//use of async will wait for this process to finish before moving forward. 
/*	
	console.log(req.user.id); 
		//gets user id from token through verify()
	console.log(req.body.courseId);
		//courseId is from request body.
*/
	 
/*
	enrollment steps:
		1. Look for user
			Push the details of the course we're trying to enroll in our users' enrollments subdocument array.save() the user document and return true to a variable if saving is successful, return the error message if we catch an error otherwise.
		2. Look for our course
			Push the details of the suer we're trying to enroll in our courses' enrollee's subdocument array. save() the user document and return true to a var if saving is successful, return error msg if we catch an error otherwise.
		3. When saving in both are successful, we send a message to the client.
*/
	if (req.user.isAdmin){
		return res.send({
			auth: "Failed",
			mesage: "Action Forbidden"
			})
	} else{

		let isUserUpdated = await User.findById(req.user.id)
			.then(user => {
				//checks user enrollments
				console.log(user.enrollments);

				//add courseId to user's enrollment array:
				user.enrollments.push({courseId: req.body.courseId})
					//adds the whole subdoc with just the courseId
				//return the value of saving the document
				return user.save()
				.then(user => true) //assigns true for isUserUpdated
				.catch(err => err.message) 
			})

			console.log(isUserUpdated);
			//end the request/response when isUserUpdated does not return true.
			if(isUserUpdated !== true) return res.send(isUserUpdated);

		let isCourseUpdated = await Course.findById(req.body.courseId)
			.then(course => {
				//add the logged in user's id into the course's enrollees subdoc array:
				course.enrollees.push({userId: req.user.id})

				return course.save()
				.then(user => true)
				.catch(err => err.message)

			})

			console.log(isCourseUpdated);
			if(isCourseUpdated !== true) return res.send(isCourseUpdated);
			if(isUserUpdated && isCourseUpdated) return res.send("Enrolled Successfully.");
	
	}
}

module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send({
				isAvailable: true,
				message: "Email Available."
			})
		} else{
			return res.send({
				isAvailable: false,
				message: "Email is already registered."
			})
		}
	})
	.catch(err => res.send(err))

}

module.exports.getEnrollments = (req, res) => {
		/*console.log(req.user);*/
	User.findById(req.user.id)	
	.then (result => res.send(result.enrollments))
	.catch (err => res.send(err))

}

