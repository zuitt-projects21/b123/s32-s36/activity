const express = require("express");

const router = express.Router();

const userControllers = require('../controllers/userControllers');

const {
	registerUserController,
	loginUser,
	viewUser,
	updateProfile,
	enroll,
	checkEmailExists,
	getEnrollments
} = userControllers;

const auth = require('../auth');
//verify is a method
const {verify} = auth;
/*console.log(userControllers);*/

//add user
router.post('/',registerUserController);
//login user
router.post('/login',loginUser);

//getSingleUser
	//route methods, much like middleware, give access to req,res and next objects for the functions that are included in them.
	//in fact, in ExpressJS, we can add multiple layes of middleware to do tasks before letting another function perform another task.
router.get('/getUserDetails',verify,viewUser);

//update user - fn, ls, mobile only
router.put('/updateProfile',verify,updateProfile);

//enroll route
router.post('/enroll',verify,enroll)

//check email exist route
router.post('/checkEmailExists',checkEmailExists);

//get Enrollments list
router.get('/getEnrollments',verify,getEnrollments);

			
module.exports = router;