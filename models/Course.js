const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({

		name: {
			type: String,
			required: [true, "Name is required"]
		},
		description: {
			type: String,
			required: [true, "Description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
			//creates a date on the system time.
		},
		/*In mongoDB, we can have 2 way embedding for models with many to many relationships. Each subdocument array also has a schema for its documents*/
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "user id is required."]
				},
				enrolledOn:{
					type: Date,
					default: new Date()
				},
				status:{
					type: String,
					default: "Enrolled"
				}
			}
		]

	});

module.exports = mongoose.model("Course", courseSchema);