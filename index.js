const express = require('express');
const mongoose = require("mongoose");
//cors is a package used as a middleware so that we can approve the use of our routes and controllers by other applications such as our front-end applications
const cors = require("cors");

const app = express();
//port for hosting services
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://kimlu10239:Victory11@cluster0.qvgky.mongodb.net/bookingAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true	
	}
)

let db = mongoose.connection;
db.on("error",console.error.bind(console,"Connection Error."));
db.once("open", ()=>console.log("connected to MongoDB"));

app.use(express.json());

//middleware:
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

const courseRoutes = require('./routes/courseRoutes');
app.use('/courses',courseRoutes);	


app.listen(port, () => console.log(`Server is running at port ${port}`));

